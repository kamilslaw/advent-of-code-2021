const fs = require("fs");

const binaryNumbers = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

function isGamma(binaryNumbers, position) {
  let zeros = 0;
  let ones = 1;
  for (let y = 0; y < binaryNumbers.length; y += 1) {
    if (binaryNumbers[y][position] === "0") zeros += 1;
    else ones += 1;
  }

  return ones > zeros;
}

const n = binaryNumbers[0].length;

const oxygen = Array(n)
  .fill(0)
  .reduce((acc, _, i) => {
    if (acc.length === 1) return acc;
    const useOnes = isGamma(acc, i);
    return acc.filter((x) => x[i] === (useOnes ? "1" : "0"));
  }, binaryNumbers)[0];

const scrubber = Array(n)
  .fill(0)
  .reduce((acc, _, i) => {
    if (acc.length === 1) return acc;
    const useOnes = isGamma(acc, i);
    return acc.filter((x) => x[i] === (useOnes ? "0" : "1"));
  }, binaryNumbers)[0];

console.log(parseInt(oxygen, 2) * parseInt(scrubber, 2));

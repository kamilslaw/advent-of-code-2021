const fs = require("fs");

const binaryNumbers = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

const n = binaryNumbers[0].length;
let gamma = "";
let epsilon = "";

for (let x = 0; x < n; x += 1) {
  let zeros = 0;
  let ones = 1;
  for (let y = 0; y < binaryNumbers.length; y += 1) {
    if (binaryNumbers[y][x] === "0") zeros += 1;
    else ones += 1;
  }
  if (ones > zeros) {
    gamma += "1";
    epsilon += "0";
  } else {
    gamma += "0";
    epsilon += "1";
  }
}

console.log(gamma);
console.log(epsilon);
console.log(parseInt(gamma, 2) * parseInt(epsilon, 2));

const fs = require("fs");

const depths = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((x) => parseInt(x));

let result = 0;
for (let i = 3; i < depths.length; i += 1) {
  const oldWindowFirstElement = depths[i - 3];
  const newWindowLastElement = depths[i];
  if (newWindowLastElement > oldWindowFirstElement) {
    result += 1;
  }
}

console.log(result);

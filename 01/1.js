const fs = require("fs");

const depths = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map(x => parseInt(x));

let result = 0;
for (let i = 1; i < depths.length; i += 1) {
  if (depths[i] > depths[i - 1]) {
    result += 1;
  }
}

console.log(result);

const fs = require("fs");

const data = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((s) => !!s);
const lines = data.map(parse);
const maxX = Math.max(...lines.map((l) => Math.max(l.x1, l.x2)));
const maxY = Math.max(...lines.map((l) => Math.max(l.y1, l.y2)));

const hits = Array(maxY + 1).fill(undefined);
for (let i = 0; i <= maxY; i += 1) hits[i] = Array(maxX + 1).fill(0);

lines.forEach((line) => {
  const x1 = Math.min(line.x1, line.x2);
  const x2 = Math.max(line.x1, line.x2);
  const y1 = Math.min(line.y1, line.y2);
  const y2 = Math.max(line.y1, line.y2);

  if (x1 === x2) for (let i = y1; i <= y2; i += 1) hits[i][x1] += 1;
  else if (y1 === y2) for (let i = x1; i <= x2; i += 1) hits[y1][i] += 1;
  else
    for (let i = 0; i <= y2 - y1; i += 1)
      hits[line.y1 > line.y2 ? line.y1 - i : line.y1 + i][
        line.x1 > line.x2 ? line.x1 - i : line.x1 + i
      ] += 1;
});

const result = hits.flatMap((row) => row.filter((i) => i > 1)).length;
console.log(result);

function parse(str) {
  const pairs = str.split(" -> ");
  const xy1 = pairs[0].split(",").map((s) => parseInt(s));
  const xy2 = pairs[1].split(",").map((s) => parseInt(s));
  return { x1: xy1[0], y1: xy1[1], x2: xy2[0], y2: xy2[1] };
}

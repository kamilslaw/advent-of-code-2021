const fs = require("fs");

const data = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((s) => !!s);
const lines = data.map(parse).filter((l) => l.x1 === l.x2 || l.y1 === l.y2);
const maxX = Math.max(...lines.map((l) => l.x2));
const maxY = Math.max(...lines.map((l) => l.y2));

const hits = Array(maxY + 1).fill(undefined);
for (let i = 0; i <= maxY; i += 1) hits[i] = Array(maxX + 1).fill(0);

lines.forEach((line) => {
  if (line.x1 === line.x2)
    for (let i = line.y1; i <= line.y2; i += 1) hits[i][line.x1] += 1;
  else for (let i = line.x1; i <= line.x2; i += 1) hits[line.y1][i] += 1;
});

const result = hits.flatMap((row) => row.filter((i) => i > 1)).length;
console.log(result);

function parse(str) {
  const pairs = str.split(" -> ");
  const xy1 = pairs[0].split(",").map((s) => parseInt(s));
  const xy2 = pairs[1].split(",").map((s) => parseInt(s));
  return {
    x1: Math.min(xy1[0], xy2[0]),
    y1: Math.min(xy1[1], xy2[1]),
    x2: Math.max(xy1[0], xy2[0]),
    y2: Math.max(xy1[1], xy2[1]),
  };
}

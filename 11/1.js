const fs = require("fs");

const STEPS = 100;

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);
const length = lines[0].length;
const octopuses = lines.flatMap((o) => [...o].map((x) => parseInt(x)));

let flashes = 0;

for (let _ = 0; _ < STEPS; _ += 1) {
  const set = new Set();
  for (let i = 0; i < octopuses.length; i += 1) octopuses[i] += 1;
  for (let i = 0; i < octopuses.length; i += 1)
    if (octopuses[i] >= 10 && !set.has(i)) flash(i, set);
  for (let i = 0; i < octopuses.length; i += 1)
    if (octopuses[i] >= 10) {
      flashes += 1;
      octopuses[i] = 0;
    }
}

console.log(flashes);

function flash(i, set) {
  if (set.has(i)) return;
  set.add(i);
  const neighbours = getNeighbours(i);
  neighbours.forEach((n) => (octopuses[n] += 1));
  neighbours.filter((n) => octopuses[n] >= 10).forEach((n) => flash(n, set));
}

function getNeighbours(index) {
  const result = [];
  if (index >= length) result.push(index - length); // UP
  if (index < octopuses.length - length) result.push(index + length); // DOWN
  if (index % length !== 0) result.push(index - 1); // LEFT
  if (index % length !== length - 1) result.push(index + 1); // RIGHT

  if (index >= length && index % length !== 0) result.push(index - length - 1); // UP-LEFT
  if (index >= length && index % length !== length - 1)
    result.push(index - length + 1); // UP-RIGHT
  if (index < octopuses.length - length && index % length !== 0)
    result.push(index + length - 1); // DOWN-LEFT
  if (index < octopuses.length - length && index % length !== length - 1)
    result.push(index + length + 1); // DOWN-RIGHT

  return result;
}

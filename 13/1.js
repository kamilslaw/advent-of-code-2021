const fs = require("fs");

const parts = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);
const positions = parts[0]
  .split(/\r?\n/)
  .map((str) => str.split(",").map((x) => parseInt(x)));
const instructions = parts[1]
  .split(/\r?\n/)
  .map((str) => ({ x: str.includes("x"), v: parseInt(str.split("=")[1]) }));

let transparent = getTransparent(positions);
transparent = fold(transparent, instructions[0]);
console.log(count(transparent));

function count(transparent) {
  return toPositions(transparent).length;
}

function getTransparent(positions) {
  const x = Math.max(...positions.map((p) => p[0])) + 1;
  const y = Math.max(...positions.map((p) => p[1])) + 1;
  const result = Array(x);
  for (let i = 0; i < x; i += 1) result[i] = Array(y).fill(0);
  for (const p of positions) result[p[0]][p[1]] = 1;
  return result;
}

function toPositions(transparent) {
  const result = [];
  for (let i = 0; i < transparent.length; i += 1)
    for (let j = 0; j < transparent[i].length; j += 1)
      if (transparent[i][j] === 1) result.push([i, j]);
  return result;
}

function fold(transparent, instruction) {
  if (instruction.x)
    return getTransparent(
      toPositions(transparent).map((p) => [
        p[0] < instruction.v ? p[0] : instruction.v - (p[0] - instruction.v),
        p[1],
      ])
    );
  else
    return getTransparent(
      toPositions(transparent).map((p) => [
        p[0],
        p[1] < instruction.v ? p[1] : instruction.v - (p[1] - instruction.v),
      ])
    );
}

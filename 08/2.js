// 1: ["c", "f"],
// 7: ["a", "c", "f"],
// 4: ["b", "c", "d", "f"],
// 8: ["a", "b", "c", "d", "e", "f", "g"],
// 2: ["a", "c", "d", "e", "g"],
// 3: ["a", "c", "d", "f", "g"],
// 5: ["a", "b", "d", "f", "g"],
// 0: ["a", "b", "c", "e", "f", "g"],
// 6: ["a", "b", "d", "e", "f", "g"],
// 9: ["a", "b", "c", "d", "f", "g"],
const fs = require("fs");

const codesCollection = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((line) => line.split(" | ")[0].split(" "));
const wordsCollection = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((line) => line.split(" | ")[1].split(" "));

let sum = 0;

for (let i = 0; i < codesCollection.length; i += 1) {
  const mapping = getMapping(codesCollection[i]);
  const digits = wordsCollection[i]
    .map((word) => getDigit(word, mapping))
    .join("");
  sum += parseInt(digits);
}

console.log(sum);

function getDigit(word, mapping) {
  if (word.length === 2) return 1;
  if (word.length === 3) return 7;
  if (word.length === 4) return 4;
  if (word.length === 7) return 8;
  if (word.length === 5) {
    if (word.includes(mapping.e)) return 2;
    if (word.includes(mapping.b)) return 5;
    return 3;
  }
  if (word.length === 6) {
    if (!word.includes(mapping.e)) return 9;
    if (word.includes(mapping.d)) return 6;
    return 0;
  }
}

function getMapping(codes) {
  // 'a' is the only letter that exist in 7s but not in 1s
  const a = single([...by(codes, 2), ...by(codes, 3)])[0];
  // 'g' is in all 5s; the same as 'a', but we can filter it out; the same as 'd' but it exists in 4s also
  const g = alwaysOccur([...by(codes, 5)])
    .filter((l) => l !== a)
    .filter((l) => ![...by(codes, 4)].some((code) => code.includes(l)))[0];
  // 'd' is in all 5s; the same as 'a', but we can filter it out; the same as 'gg' but it does not exist in 4s
  const d = alwaysOccur([...by(codes, 5)])
    .filter((l) => l !== a)
    .filter((l) => [...by(codes, 4)].some((code) => code.includes(l)))[0];
  // 'b' is in 4s; we know 'd' already, there is 'c' & 'f' left, but they exist in 2s and 'b' not
  const b = alwaysOccur([...by(codes, 4)])
    .filter((l) => l !== d)
    .filter((l) => ![...by(codes, 2)].some((code) => code.includes(l)))[0];
  // 'f' exists in every 6s; the same for 'a', 'b' and 'g', but we know them already
  const f = alwaysOccur([...by(codes, 6)])
    .filter((l) => l !== a)
    .filter((l) => l !== b)
    .filter((l) => l !== g)[0];
  // 'c' exists in every 2s; the same for 'f', but we know it already
  const c = alwaysOccur([...by(codes, 2)]).filter((l) => l !== f)[0];
  // 'e' is last one left; we can filter it out from 7s
  const e = alwaysOccur([...by(codes, 7)])
    .filter((l) => l !== a)
    .filter((l) => l !== b)
    .filter((l) => l !== c)
    .filter((l) => l !== d)
    .filter((l) => l !== f)
    .filter((l) => l !== g)[0];

  return { a, b, c, d, e, f, g };
}

function by(collection, length) {
  return collection.filter((el) => el.length === length);
}

function single(collections) {
  const counts = {};
  for (const el of collections.join("")) counts[el] = (counts[el] || 0) + 1;
  return Object.keys(counts).filter((k) => counts[k] === 1);
}

function alwaysOccur(collections) {
  const counts = {};
  for (const el of collections.join(""))
    counts[el] = counts[el] ? counts[el] + 1 : 1;
  return Object.keys(counts).filter((k) => counts[k] === collections.length);
}

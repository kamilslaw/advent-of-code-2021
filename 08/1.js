const fs = require("fs");

const words = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .flatMap((line) => line.split(" | ")[1].split(" "));

console.log(
  words.filter(
    (w) => w.length === 2 || w.length === 3 || w.length === 4 || w.length === 7
  ).length
);

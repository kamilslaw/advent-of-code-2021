const fs = require("fs");

const SCORES = {
  ")": 3,
  "]": 57,
  "}": 1197,
  ">": 25137,
};
const CHUNKS = {
  "(": ")",
  "[": "]",
  "{": "}",
  "<": ">",
};
const CHUNK_OPENS = Object.keys(CHUNKS);

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((str) => [...str]);

let score = 0;
for (const line of lines) {
  const corruptedCharacter = getCorruptedCharacter(line);
  if (corruptedCharacter) score += SCORES[corruptedCharacter];
}

console.log(score);

function getCorruptedCharacter(line) {
  const stack = [line[0]];
  for (let i = 1; i < line.length; i += 1) {
    const c = line[i];
    if (CHUNK_OPENS.includes(c)) stack.push(c);
    else if (c === CHUNKS[stack[stack.length - 1]]) stack.pop();
    else return c;
  }
}

const fs = require("fs");

const SCORES = {
  ")": 1,
  "]": 2,
  "}": 3,
  ">": 4,
};
const CHUNKS = {
  "(": ")",
  "[": "]",
  "{": "}",
  "<": ">",
};
const CHUNK_OPENS = Object.keys(CHUNKS);

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((str) => [...str])
  .filter((line) => !isCorrupted(line));

const scores = lines
  .map(getMissingPart)
  .map(getScore)
  .sort((a, b) => a - b);

console.log(scores[Math.floor(scores.length / 2)]);

function isCorrupted(line) {
  const stack = [line[0]];
  for (let i = 1; i < line.length; i += 1) {
    const c = line[i];
    if (CHUNK_OPENS.includes(c)) stack.push(c);
    else if (c === CHUNKS[stack[stack.length - 1]]) stack.pop();
    else return true;
  }
  return false;
}

function getMissingPart(line) {
  const stack = [line[0]];
  for (let i = 1; i < line.length; i += 1) {
    const c = line[i];
    if (CHUNK_OPENS.includes(c)) stack.push(c);
    else if (c === CHUNKS[stack[stack.length - 1]]) stack.pop();
    else throw new Error("corrupted line should not happend here!");
  }

  return stack.reverse().map((c) => CHUNKS[c]);
}

function getScore(missing) {
  let score = 0;
  missing.forEach((m) => {
    score *= 5;
    score += SCORES[m];
  });
  return score;
}

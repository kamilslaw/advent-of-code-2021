const fs = require("fs");

const data = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);

const draws = data[0].split(",").map((x) => parseInt(x));

let boards = data.slice(1).map((str) => {
  const lines = str.split(/\r?\n/).filter((l) => !!l);
  return lines.map((line) =>
    line
      .split(" ")
      .filter((x) => x === "0" || !!x)
      .map((x) => [parseInt(x), false])
  );
});

let lastWinnerIndex = -1;

for (draw of draws) {
  boards = boards.map((b) => updateBoard(b, draw));
  const numberOfWinners = boards.filter(bingo).length;
  const lastStanding = numberOfWinners === boards.length - 1;

  if (lastStanding) {
    lastWinnerIndex = boards.findIndex((b) => !bingo(b));
  } else if (lastWinnerIndex >= 0) {
    console.log(getBoardScore(boards[lastWinnerIndex], draw));
    return;
  }
}

function bingo(board) {
  return horizontalBingo(board) || vertivalBingo(board);
}

function horizontalBingo(board) {
  return board.some((line) => line.every((x) => x[1]));
}

function vertivalBingo(board) {
  for (let i = 0; i < board[0].length; i += 1)
    if (board.every((line) => line[i][1])) return true;
  return false;
}

function updateBoard(board, value) {
  return [
    ...board.map((line) => [
      ...line.map((x) => [x[0], x[0] === value ? true : x[1]]),
    ]),
  ];
}

function getBoardScore(board, draw) {
  let unmarked = 0;
  board.forEach((line) =>
    line.forEach((x) => {
      if (!x[1]) unmarked += x[0];
    })
  );
  return unmarked * draw;
}

const fs = require("fs");

const days = 256;
let agesList = fs
  .readFileSync("input.txt", "utf-8")
  .split(",")
  .map((a) => parseInt(a));

let ages = {};
for (let i = 0; i <= 8; i += 1)
  ages[i] = agesList.filter((a) => a === i).length;

for (let _ of Array(days).fill(0)) {
  newAges = {};
  for (let i = 1; i <= 8; i += 1) newAges[i - 1] = ages[i];
  newAges[8] = ages[0];
  newAges[6] += ages[0];
  ages = newAges;
}

console.log(Object.values(ages).reduce((acc, x) => acc + x, 0));

const fs = require("fs");

const days = 80;
let ages = fs
  .readFileSync("input.txt", "utf-8")
  .split(",")
  .map((a) => parseInt(a));

for (let _ of Array(days).fill(0)) {
  const newAges = [];
  for (let i = 0; i < ages.length; i += 1) {
    ages[i] -= 1;
    if (ages[i] < 0) {
      newAges.push(8);
      ages[i] = 6;
    }
  }
  ages = ages.concat(newAges);
}

console.log(ages.length);

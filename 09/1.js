const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

const length = lines[0].length;
const heightmap = [...lines.join("")].map((x) => parseInt(x));
let result = 0;

for (let i = 0; i < heightmap.length; i += 1)
  if (
    getNeighbours(i)
      .map((n) => heightmap[n])
      .every((n) => n > heightmap[i])
  )
    result += heightmap[i] + 1;

console.log(result);

function getNeighbours(index) {
  const result = [];
  if (index >= length) result.push(index - length);
  if (index < heightmap.length - length) result.push(index + length);
  if (index % length !== 0) result.push(index - 1);
  if (index % length !== length - 1) result.push(index + 1);
  return result;
}

const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

const length = lines[0].length;
const heightmap = [...lines.join("")].map((x) => (parseInt(x) === 9 ? -1 : 0));

for (let i = 0; i < heightmap.length; i += 1) paint(i, i + 1);
const groups = {};
heightmap
  .filter((h) => h !== -1)
  .forEach((h) => (groups[h] = (groups[h] || 0) + 1));
console.log(
  Object.values(groups)
    .sort((a, b) => b - a)
    .slice(0, 3)
    .reduce((acc, x) => acc * x, 1)
);

function paint(i, color) {
  if (heightmap[i] !== 0) return;
  heightmap[i] = color;
  getNeighbours(i).forEach((n) => paint(n, color));
}

function getNeighbours(index) {
  const result = [];
  if (index >= length) result.push(index - length);
  if (index < heightmap.length - length) result.push(index + length);
  if (index % length !== 0) result.push(index - 1);
  if (index % length !== length - 1) result.push(index + 1);
  return result;
}

const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);
const pairs = lines.map(getPair);
const vertices = getVertices(pairs);
const graph = {};
for (const vertex of vertices)
  graph[vertex] = vertices.filter(
    (v) =>
      v !== vertex && pairs.some((p) => p.includes(v) && p.includes(vertex))
  );

const paths = walk([], "start", 0);
console.log(paths.length);

function walk(path, vertex, depth) {
  if (vertex === "end") return [[...path, "end"]];
  if (depth === 200) return [null];
  return graph[vertex]
    .filter((v) => v !== "start" && (!isSmall(v) || !path.includes(v)))
    .flatMap((v) => walk([...path, vertex], v, depth + 1))
    .filter((p) => p !== null);
}

function getPair(str) {
  const vertices = str.split("-");
  return [vertices[0], vertices[1]];
}

function getVertices(pairs) {
  return [...new Set(pairs.flat())];
}

function isSmall(v) {
  return v.toLowerCase() === v;
}

const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);
const pairs = lines.map(getPair);
const vertices = getVertices(pairs);
const graph = {};
for (const vertex of vertices)
  graph[vertex] = vertices.filter(
    (v) =>
      v !== vertex && pairs.some((p) => p.includes(v) && p.includes(vertex))
  );

const paths = walk([], "start", 0);
console.log(paths.length); // some of the paths are for some reason wrong !! have to be check again here
console.log(paths.filter(p => {
  const smalls = p.slice(1).filter(isSmall);
  return new Set(smalls).size >= smalls.length - 1;
}).length);

function walk(path, vertex, depth) {
  if (vertex === "end") return [[...path, "end"]];
  if (depth === 1000) return [null];
  return graph[vertex]
    .filter((v) => allow(path, v))
    .flatMap((v) => walk([...path, vertex], v, depth + 1))
    .filter((p) => p !== null);
}

function allow(path, v) {
  if (v === "start") return false;
  if (!isSmall(v)) return true;
  const smalls = path.filter(isSmall);
  return (
    !smalls.includes(v) || // not in path yet
    new Set(smalls).size === smalls.length // once in path, but no other small caves are twice
  );
}

function getPair(str) {
  const vertices = str.split("-");
  return [vertices[0], vertices[1]];
}

function getVertices(pairs) {
  return [...new Set(pairs.flat())];
}

function isSmall(v) {
  return v.toLowerCase() === v;
}

const fs = require("fs");

const STEPS = 10;
const parts = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);
const polymer = [...parts[0]];
const rules = parts[1].split(/\r?\n/).map((str) => str.split(" -> "));

const finalPolymer = Array(STEPS).fill(0).reduce(react, polymer);
console.log(calculateValue(finalPolymer));

function react(polymer) {
  const newPolymer = [];
  for (let i = 0; i < polymer.length - 1; i += 1) {
    const pair = polymer[i] + polymer[i + 1];
    newPolymer.push(polymer[i]);
    const rule = rules.find((r) => r[0] === pair);
    if (rule) newPolymer.push(rule[1]);
  }
  newPolymer.push(polymer[polymer.length - 1]);
  return newPolymer;
}

function calculateValue(polymer) {
  const obj = {};
  polymer.forEach((el) => (obj[el] = (obj[el] || 0) + 1));
  const max = Math.max(...Object.values(obj));
  const min = Math.min(...Object.values(obj));
  return max - min;
}

const fs = require("fs");

const HALF_WAY = 20;
const END_WAY = 20;

const parts = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);
const polymer = [...parts[0]];
const rules = Object.fromEntries(
  parts[1].split(/\r?\n/).map((str) => str.split(" -> "))
);

const letters = [
  ...new Set([
    ...Object.values(rules),
    ...Object.keys(rules).flatMap((r) => [...r]),
  ]),
].filter((l) => !!l);
const pairsResults = {};
letters.forEach((l) =>
  letters.forEach((r) => (pairsResults[l + r] = getForPairToHalf(l, r)))
);

const halfWayPolymer = Array(HALF_WAY).fill(0).reduce(react, polymer); // we have polymer after 20 steps
// pairs results contains 20 steps for each possible pair, we only have to sum it up
const result = {};
letters.forEach((l) => (result[l] = 0));
result[halfWayPolymer[halfWayPolymer.length - 1]] = 1; // we add last element, because no pair is considering it
for (let i = 0; i < halfWayPolymer.length - 1; i += 1) {
  const partial = pairsResults[halfWayPolymer[i] + halfWayPolymer[i + 1]];
  letters.forEach((l) => (result[l] += partial[l] || 0));
}

console.log(calculateValue(result));

function react(polymer) {
  const newPolymer = [];
  for (let i = 0; i < polymer.length - 1; i += 1) {
    newPolymer.push(polymer[i]);
    const rule = rules[polymer[i] + polymer[i + 1]];
    if (rule) newPolymer.push(rule);
  }
  newPolymer.push(polymer[polymer.length - 1]);
  return newPolymer;
}

function calculateValue(obj) {
  const o = Object.entries(obj).sort((a, b) => a[1] - b[1]);
  return [o[o.length - 1], o[0], o[o.length - 1][1] - o[0][1]];
}

function getValuesObj(polymer) {
  const obj = {};
  polymer.forEach((el) => (obj[el] = (obj[el] || 0) + 1));
  return obj;
}

function getForPairToHalf(l, r) {
  const obj = getValuesObj(Array(END_WAY).fill(0).reduce(react, [l, r]));
  obj[r] -= 1; // remove last character, because it will be in the next pair
  return obj;
}

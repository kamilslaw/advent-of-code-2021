const fs = require("fs");

const commands = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((x) => x.split(" "))
  .map((x) => [x[0], parseInt(x[1])]);

let horizontal = 0;
let depth = 0;

commands.forEach((command) => {
  if (command[0] === "forward") horizontal += command[1];
  else if (command[0] === "down") depth += command[1];
  else if (command[0] === "up") depth -= command[1];
});

console.log(horizontal * depth);

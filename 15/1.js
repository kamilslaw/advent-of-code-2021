const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);
const length = lines.length; // it is a square, so lines[0].length would work too
const caves = lines.flatMap((l) => [...l].map((x) => parseInt(x)));

console.log(dijkstra(0, caves.length - 1, caves));

function dijkstra(start, end, graph) {
  const d = graph.map(() => Number.MAX_SAFE_INTEGER);
  const visited = graph.map(() => false);
  d[start] = 0;
  const queue = [[start, 0]];
  visited[start] = true;
  while (queue.length) {
    const v = queue.pop()[0];
    getNeighbours(v).forEach((n) => {
      const cost = d[v] + graph[n];
      if (d[n] > cost) d[n] = cost;
      if (!visited[n]) {
        visited[n] = true;
        addSorted(queue, n, d[n]);
      }
    });
  }
  return d[end];
}

function addSorted(queue, v, cost) {
  if (!queue.length) queue.push([v, cost]);
  else
    for (let i = queue.length - 1; i >= 0; i -= 1)
      if (cost < queue[i][1]) {
        queue.splice(i + 1, 0, [v, cost]);
        return;
      }
  queue.unshift([v, cost]);
}

//console.log(bellmanFord(0, caves.length - 1, caves));

// function bellmanFord(start, end, graph) {
//   const d = graph.map(() => Number.MAX_SAFE_INTEGER);
//   d[start] = 0;
//   for (let i = 0; i < graph.length; i += 1)
//     for (let j = 0; j < graph.length; j += 1)
//       getNeighbours(j).forEach((n) => {
//         const cost = d[j] + graph[n];
//         if (d[n] > cost) d[n] = cost;
//       });
//   return d[end];
// }

function getNeighbours(index) {
  const result = [];
  if (index >= length) result.push(index - length);
  if (index < caves.length - length) result.push(index + length);
  if (index % length !== 0) result.push(index - 1);
  if (index % length !== length - 1) result.push(index + 1);
  return result;
}

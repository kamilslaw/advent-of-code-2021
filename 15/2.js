const fs = require("fs");

const SCALE = 5;

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);
const cavesArray = lines.map((l) => [...l].map((x) => parseInt(x)));
const length = lines.length * SCALE;
const biggerCavesArray = generateDungeonsMyMaster(cavesArray, SCALE);
const biggerCaves = biggerCavesArray.flat();

console.log(
  biggerCaves.length,
  dijkstra(0, biggerCaves.length - 1, biggerCaves)
);

function dijkstra(start, end, graph) {
  const d = graph.map(() => Number.MAX_SAFE_INTEGER);
  const visited = graph.map(() => false);
  d[start] = 0;
  const queue = [[start, 0]];
  visited[start] = true;
  while (queue.length) {
    const v = queue.pop()[0];
    getNeighbours(v).forEach((n) => {
      const cost = d[v] + graph[n];
      if (d[n] > cost) d[n] = cost;
      if (!visited[n]) {
        visited[n] = true;
        addSorted(queue, n, d[n]);
      }
    });
  }
  return d[end];
}

function addSorted(queue, v, cost) {
  if (!queue.length) queue.push([v, cost]);
  else
    for (let i = queue.length - 1; i >= 0; i -= 1)
      if (cost < queue[i][1]) {
        queue.splice(i + 1, 0, [v, cost]);
        return;
      }
  queue.unshift([v, cost]);
}

function getNeighbours(index) {
  const result = [];
  if (index >= length) result.push(index - length);
  if (index < biggerCaves.length - length) result.push(index + length);
  if (index % length !== 0) result.push(index - 1);
  if (index % length !== length - 1) result.push(index + 1);
  return result;
}

function generateDungeonsMyMaster(array, scale) {
  const result = Array(length).fill([]);
  for (let l = 0; l < length; l += 1) result[l] = Array(length).fill(-1);
  for (let sx = 0; sx < scale; sx += 1)
    for (let sy = 0; sy < scale; sy += 1)
      for (let x = 0; x < array.length; x += 1)
        for (let y = 0; y < array.length; y += 1) {
          const value = (array[x][y] + sx + sy) % 9;
          result[array.length * sx + x][array.length * sy + y] = value || 9; // if 0, then 9
        }

  return result;
}

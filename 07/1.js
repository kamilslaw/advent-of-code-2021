const fs = require("fs");

const positions = fs
  .readFileSync("input.txt", "utf-8")
  .split(",")
  .map((a) => parseInt(a));

const maxPosition = Math.max(...positions);
let minFuel = Number.MAX_SAFE_INTEGER;

for (let i = 0; i <= maxPosition; i += 1) {
  let currentFuel = 0;
  for (const position of positions) currentFuel += Math.abs(position - i);
  if (currentFuel < minFuel) minFuel = currentFuel;
}

console.log(minFuel);
